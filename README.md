# README #

This repository contains the chart created to plot a graph with sample data received from an API using Chart.JS library and Javascript. This chart was then implemented on the Symfony2 application dashboard developed for Procept (Pty) Ltd. 

### Contribution ###

* All of the files in this repository were created by myself


### Functionality ###

* Read JSON object received from API and store in an array
* Draw graph in random colors to please the eye


### Contact ###

* For information contact Azmi at mafareed@gmail.com