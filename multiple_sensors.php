<!doctype html>
<html>
	<head>
		<title>Line Chart</title>
		<script src="js/Chart.js"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	</head>
	<body>
		<div style="width:50%">
			<div>
				<canvas id="canvas" height="600" width="800"></canvas>
			</div>
		</div>

	<script>
		//Function to add leading zero to number
		function pad(num, size) {
		    var s = num+"";
		    while (s.length < size) s = "0" + s;
		    return s;
		}		
	
	
		var jsonData = '{ "sensors" : [{ "sensor": [{"timestamp": "1430784776", "temperature": "13.4", "ignore": "false"}, {"timestamp": "1430788376", "temperature": "18.2", "ignore": "false"}, {"timestamp": "1430791976", "temperature": "23.4", "ignore": "true"},{"timestamp": "1430795576", "temperature": "45.8", "ignore": "true"}, {"timestamp": "1430799176", "temperature": "48.2", "ignore": "true"}, {"timestamp": "1430802776", "temperature": "53.6", "ignore": "false"}, {"timestamp": "1430806376", "temperature": "35.9", "ignore": "false"}, {"timestamp": "1430809976", "temperature": "30.4", "ignore": "false"}, {"timestamp": "1430813576", "temperature": "28.4", "ignore": "false"}, {"timestamp": "1430817176", "temperature": "29.2", "ignore": "false"}, {"timestamp": "1430820776", "temperature": "44.4", "ignore": "false"}, {"timestamp": "1430824376", "temperature": "62.8", "ignore": "false"}] }, { "sensor": [{"timestamp": "1430817176", "temperature": "29.2", "ignore": "false"}, {"timestamp": "1430820776", "temperature": "44.4", "ignore": "false"}, {"timestamp": "1430824376", "temperature": "62.8", "ignore": "false"}, {"timestamp": "1430827976", "temperature": "85.4", "ignore": "false"}, {"timestamp": "1430831576", "temperature": "93.4", "ignore": "false"}, {"timestamp": "1430835176", "temperature": "95.6", "ignore": "false"}, {"timestamp": "1430838776", "temperature": "85.4", "ignore": "false"}, {"timestamp": "1430842376", "temperature": "80.8", "ignore": "false"}, {"timestamp": "1430845976", "temperature": "74.8", "ignore": "false"}, {"timestamp": "1430849576", "temperature": "62.5", "ignore": "false"}, {"timestamp": "1430853176", "temperature": "43.7", "ignore": "false"}, {"timestamp": "1430856776", "temperature": "34.4", "ignore": "false"}] } ]}';
		
		var obj = JSON.parse(jsonData);
		
		var sensors = [];
		var ignoreSensors = [];

		//Get each sensor element from JSON array object
		for(var j=0; j < obj.sensors.length; j++ )
		{
			var time = [];
			var temperature = [];
			
			var ignoreTemperature = [];
			//Get dat and temperature for each sensor element
			for(var i=0; i < obj.sensors[j].sensor.length; i++ )
			{
				//Format time from Unix timestamp
				var d = new Date(parseInt(obj.sensors[j].sensor[i].timestamp)*1000);
				var hours = pad(d.getHours(), 2);   //Hour with leading zero paddding for single digit
				var mins =  pad(d.getMinutes(), 2); //Minute with leading zero paddding for single digit
				
				time.push(hours + ":" + mins);
				temperature.push(parseFloat(obj.sensors[j].sensor[i].temperature));
				
			}
			
			sensors[j] = [ time, temperature];
		}
		
		
		
		var randomColorFactor = function(){ return Math.round(Math.random()*255)};

		//Format line chart data for each sensor
		var chartData = [];
		for(var i=0; i < sensors.length; i++ )
		{
			var r = randomColorFactor();
			var g = randomColorFactor();
			var b = randomColorFactor();
			chartData[i] = {label: "Sensor", fillColor : 'rgba(' + r + ',' + g + ',' + b + ',0.2)', strokeColor : 'rgba(' + r + ',' + g + ',' + b + ',1)',pointColor : 'rgba(' + r + ',' + g + ',' + b + ',1)', pointStrokeColor : "#fff", pointHighlightFill : "#fff", pointHighlightStroke : 'rgba(' + r + ',' + g + ',' + b + ',1)', data : sensors[i][1] };
		}
		

		var lineChartData = {
			labels : sensors[0][0],
			datasets : chartData

		}

	//Display the charts on the page canvas
	window.onload = function(){
		var ctx = document.getElementById("canvas").getContext("2d");
		window.temperatureChart = new Chart(ctx).Line(lineChartData, {
			responsive: true
		});
		
		
	}




	</script>
	</body>
</html>
